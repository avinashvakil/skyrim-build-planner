import pygraph

class SkillManager(object):
	def __init__(self):
		self.skills = set()
		self.skillNames = {}
		
	def addSkillsFromXML(self,skillXML):
		import bs4
		xmldoc = bs4.BeautifulSoup(skillXML)
		xmldoc = xmldoc.skills
		skillsxml = xmldoc.find_all('skill')
		for skillxml in skillsxml:
			newSkill = Skill(skillxml)
			self.skills.add( newSkill )
			self.skillNames[newSkill.name] = newSkill
		
	def addSkillsFromXMLFile(self,skillXMLFile):
		afile = open(skillXMLFile, 'r')
		stuffinfile = afile.read()
		afile.close()
		self.addSkillsFromXML(stuffinfile)

class Skill(object):
	def __init__(self,skillXML):
		self.parseXML(skillXML)
		
	def parseXML(self,skillXML):
		from pygraph.classes.digraph import digraph
		self.name = skillXML.skillname.string
		self.baselevel = int(skillXML.basevalue.string)
		self.perksGraph = digraph()
		self.perks = set()
		for perkXML in skillXML.find_all('perk'):
			perkname = perkXML.perkname.string
			count = 1
			for rank in perkXML.ranks.find_all('rankrequirement'):
				perkskillreq = int(rank.string)
				if count == 1:
					perkrequiredperkstrings = map(lambda x:(x.string,1),perkXML.requiredperks.find_all('requiredperk'))
				else:
					perkrequiredperkstrings = [(perkname,count - 1)]
				perkperklevel = count
				
				self.perks.add( Perk(perkname, perkskillreq, perkrequiredperkstrings, perkperklevel) )
				
				count += 1
		#print self.perks
		map(lambda x:x.resolvePrerequisites(self.perks),self.perks)
		map(lambda x:self.perksGraph.add_node(x),self.perks)
		for perk in self.perks:
			for dependency in perk.perkreq:
				self.perksGraph.add_edge((perk,dependency))
		print self.name,self.baselevel,self.perksGraph
		
	def __str__(self):
			return "<"+self.name + " SKILL OBJECT>"
			
	def __repr__(self):
			return self.__str__()
		
class Perk(object):
	def __init__(self,name,skillreq,requiredperkstrings,perklevel):
		self.name = name
		self.skillreq = skillreq
		self.requiredperkstrings = requiredperkstrings
		self.perklevel = perklevel
		
	def resolvePrerequisites(self,otherPerkList):
		self.perkreq = set()
		for perkstring in self.requiredperkstrings:
			for otherperk in list(otherPerkList):
				found = False
				if otherperk.name == perkstring[0] and otherperk.perklevel == perkstring[1]:
					self.perkreq.add(otherperk)
					found = True
					break
			if not found:
				print 'WARNING: ',self.name,self.perklevel, "could not resolve dependency", perkstring
		
	def __str__(self):
		return self.name + " " + unicode(self.perklevel)
		
	def __repr__(self):
		return self.__str__()