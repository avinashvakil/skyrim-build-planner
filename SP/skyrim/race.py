class Race(object):
	def __init__(self,skillmanager,racexml):
		self.skillmanager = skillmanager
		self.boosts = {}
		self.parsexml(racexml)
		
	def parsexml(self,racexml):
		self.name = racexml.racename.string
		for boost in racexml.raceskillboosts.find_all('boost'):
			self.boosts[ self.skillmanager.skillNames[ boost.attrs[u'skill'] ] ] = int(boost.attrs[u'amount'])
			
	def getBoost(self,skill):
		return self.boosts.get(skill,0)
		
class RaceManager(object):
	def __init__(self,skillmanager):
		self.races = set()
		self.raceNames = {}
		self.skillmanager = skillmanager
		
	def addRacesFromXML(self,skillXML):
		import bs4
		xmldoc = bs4.BeautifulSoup(skillXML)
		xmldoc = xmldoc.races
		racesxml = xmldoc.find_all('race')
		for racexml in racesxml:
			newRace = Race(self.skillmanager,racexml)
			self.races.add( newRace )
			self.raceNames[newRace.name] = newRace

	
	def addRacesFromXMLFile(self,skillXMLFile):
		afile = open(skillXMLFile, 'r')
		stuffinfile = afile.read()
		afile.close()
		self.addRacesFromXML(stuffinfile)
