class Player(object):
	def __init__(self,skillman,race):
		self.skillman = skillman
		self.race = race
		self.skillsBase = {}
		self.skillsAdjusted = {}
		for skill in self.skillman.skills:
			self.skillsBase[skill] = self.skillsAdjusted[skill] = skill.baselevel + self.race.getBoost(skill)
		print self.skillsAdjusted
			
	def setSkill(self,skill,newValue):
		self.skillsAdjusted[skill] = newValue
		
	def getTotalLevelEXP(self):
		accum = 0
		for skill in self.skillsAdjusted.keys():
			for n in xrange(self.skillsBase[skill]+1, self.skillsAdjusted[skill]+1):
				accum+=n
		return accum
		
	def getLevel(self):
		requiredExpFxn = lambda l: (level + 3) * 25
		earnedExp = self.getTotalLevelEXP()
		level = 0
		while (earnedExp - requiredExpFxn(level)) > 0:
			earnedExp -= requiredExpFxn(level)
			level += 1
		return level+float(earnedExp)/requiredExpFxn(level),earnedExp,requiredExpFxn(level)