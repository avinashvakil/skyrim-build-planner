import SP.skyrim.skill as skill
import SP.skyrim.race as race
import SP.skyrim.player as player


skillman = skill.SkillManager()
skillman.addSkillsFromXMLFile('vanillaskillsfile.xml')
raceman = race.RaceManager(skillman)
raceman.addRacesFromXMLFile('vanillaskillsfile.xml',)
aplayer = player.Player(skillman,raceman.raceNames['Nord'])
aplayer.setSkill( skillman.skillNames['Alteration'], 44 )
print aplayer.getTotalLevelEXP()
print aplayer.getLevel()